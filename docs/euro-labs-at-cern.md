-- work still in progress!!! ---

# EURO-LABS CERN Trasnational Access

CERN offers trans-national access to several of its facilities under the Horizon Europe EC funded EURO-LABS project (Grant Agreement No 101057511). 

The general framework of the programme and information on the trans-national access in all participating Research Infrastructures (RI) is available in the project web [http://web.infn.it/EURO-LABS](https://web.infn.it/EURO-LABS).  

Below, the specific implementation for the CERN facilities in the pgoramm is provided to help the **User Groups**, the **Facility Coordinators** and the **Administrative support** in the Laboratory. 


## Modality of access

In particular, CERN provides **free of charge** the participating [facilities](./cern_facilities.md) to selected researchers or research teams (**user-groups**) from academia and research institutes. 

Access may be made available to external users either 
- **in person** when the user visits the infrastructure to make use or it, 
- **in remote** through provision to the users of scientific services without visit to the facility, <br />
Examples : testing of user equipment sent to the facility, irradiation and/or analysis of material samples, remote access to installed experiments for special data-taking
- **as virtual access** through access via communication networks to resources at the facility <br />
Examples: access to databases or computing resources and codes available via Internet, or data deposition services. 

For cases 1 and 2  the facility must setup a **user selection panel (USP)**, while for case 3 access is open to all users satisfying the RI’s access policy

## User support under TNA

The TNA Access budget is used to: 
- Provide financial support (subsistence and travel money) to users – in person access
- Cover expenses for the installation and transport of the experiments
- Cover expenses for the management and administration of the users and accesses,  advertising of the trans-national access
- Cover a fraction of the operating costs (consumables, adaptation of infrastructure) of the installation
- and **special for EURO-LABS**: to support service improvements in the facility, which must be prepared, put in place and first used in the duration of the project

## Procedures

The cycle of TNA involves several actors:

- The Group Leader of the team applying for TNA funds for their experiment or test at CERN
- The Coordinator of the faclity at CERN
- The Administrative Support for the facility, either in the group hosting the facility or in the CERN EU project office.

Information for each actor in the pages below. 

- Information for Group Leaders requesting TNA funds for their research at the CERN facilities - [here](guidelines/groupleaders.md)
- Information for Facility Coordinators - [here](guidelines/facilitycoordinators.md)
- Informaiotn  for Administrative support - [here](guidelines/administrativesupport.md)
