
# Guidelines 

The handling of the TA requests within CERN consists of three main stages:
- **initial** : application submission and approval by the facility
- **registratin** : registration of users at CERN, access permissions, and to the necessary training courses 
- **reimburesment** : payement of the allowence and if approved transport expenses

The flow diagram below indicates the various stages, details in the specific pages for [Group Leaders](./groupleaders.md), [Facility Coordinators](./facilitycoordinators.md), and [Administrative User Support, Secretariats, EU office)](administrativesupport.md) below

## Flow diagram of TA handling

```mermaid
sequenceDiagram
    title : EURO-LABS Trans-national Access
    autonumber
    participant G as GroupLeader
    participant U as User
    participant F as Facility Coordinator
    participant S as Secretariat
    
    
    rect rgb(200, 230, 255)
    G->>F: Application form (word), <br/> excel file (Request part)
    loop 
        F-->G: Scienticif Evaluation - USP <br/> entry in the schedule
        F-->>F: verify eligibility
    end
    F->>G: Excel with USP Approval
    end
    rect rgb(255, 230, 230)
    Note right of F : generation of initial documents <br/>(invLetter, TrainingSheets, mailTempl-A)
    F-->>S : Inform of new experimet
    S->>S : check status of users
    S->>G : e-mail to GL <br/> (general information, user invLetters, other documents/forms for the registration) 
    G-->>U : distribute, information and invLetters
    U->>S: send signed invLetter, other requested documents
    par Users and Secretariat
    U-->>U : travel preparation
    U-->>U : accommodation booking
    S-->>S: registration (EUPR)
    S-->>S: registration training
    end
    end
    Note over G, F : experiment
    rect rgb(230, 255, 230)
    G->>F : complete Actual columns of excel file
    activate G
    Note right of F: generation ConfBeamTime, UserTADeclarations, mailTemp-B
    F-->>G : e-mail to GL with attached ConfBeamTime  time, UserTADecl
    G-->>U : distribute UserTADecl
    G->>F : returns signed ConfBeamTime
    U-->>S : return UserTADecl signed, travel and other requested docs
    S-->>S : procceed to reimbursement
    S->> U : e-mail to confirm reimbursement
    G->>F : returns Experiment report
    deactivate G
    end
```
