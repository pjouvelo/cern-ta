# Guidelines for Facility Coordinators


1.	**Informal discussion**: You will be contacted by the User Group Leader to discuss the technical aspects and feasibility of the project and the eligibility of the user group.

2.	**Selection panel**: When you receive the EURO-LABS TA Application form (word/pdf and excel file), you will forward it to the relevant EURO-LABS User Selection Panel. 

3.	Application form acronym and storage: Following the EURO-LABS rules, you will assign an acronym to the application form and store it on CERNBox. 

4.	**Outcome of the selection**: After approval or rejection of the TA application form by the USP, you will inform the User Group Leader regarding the outcome of the selection. You should fill the columns under "USP approval" in the excel file and sent it ot the Group Leader for confirmation. You should also verify the arrival/departure dates of the team members as well as their training needs.

5. **Completed application** : The Group Leader sends you back the excel file which you place in the cernbox area to genarate the various documents.

6. **Confirmation of beam time**: At the end of the visit you should request the GL to fill the last columns of the excel file "Actual" with the exact dates of the presence of each team member to the facility. 

7.	**TA report**: Remind the GL the requirement to fill within the two weeeks after thevisit the TA report containing the objectives and achievements of the experiment. 

8.	**Publication of results**: You will remind the User Group Leader that they are required to publish the results with the acknowledgement.

9.	**TA database**: To keep track of all the details of the TA activity in the facility, you will keep the following information in a database file stored in the cernbox area:
    - USP members
    -	Projects submitted (Period, TA project acronym, project title, objectives, achievements, difficulties, discipline, information on the completion of the project) 
    -	Users information ( Name, gender, nationality, home institution name, legal status and country) 
    -	Publications (year, authors, title, references, publication type, peer reviewed or not, DOI, Open access, link, acknowledgement)

10.	**Period Reporting**: During Period reporting, you will be asked by the Project Coordinator (INFN) to fill a Scientific and Technical report and to send them the TA database filled and updated. 

