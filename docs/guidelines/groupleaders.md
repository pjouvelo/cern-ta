# Guidelines for Applicants

Information for Group Leaders of Spokesperons of team wishing to receive EURO-LABS TA Support

## Group Leaders

1.	**Informal Contact**: As User Group Leaders you can contact the facility coordinator before beginning the formal application process in order to discuss the technical aspects and feasibility of the project and the eligibility of the user group.

2.	**Application form**: When details have been discussed with the Facility coordinator, you can download, complete and send the EURO-LABS TA application form to the facility coordinator (list [here](../cern_facilities.md)). 

    This involves two documents : 

    > - an application form describing the project and its objectives, available from the EURO-LABS project [web](https://web.infn.it/EURO-LABS/) [Transnational Access](https://web.infn.it/EURO-LABS/how-to-apply-for-transnational-access/) available as [word](https://istnazfisnucl-my.sharepoint.com/:w:/g/personal/giacomel_infn_it/EeDsaQ5MO_lImBqafpeLt0kBuXZAm7Wa67Pf2eqgKM75iw?e=zoq0yR) or [pdf](https://istnazfisnucl-my.sharepoint.com/:b:/g/personal/giacomel_infn_it/EdJRvAa_7DZGm0TE8VrVhgMBpmHnERyI5f2f1TaaovjrMA?e=JyOMVM) files. 
    > - an excel file, available here [TA-application-data_template.xlsx](./TA-application-data_template.xlsx) providing the information for all group participants and their travel details. In this initial phase only the columns up to the "TNA Request" need to be filled.
    
3.	**User Selection Panel**: The EURO-LABS TA application form will be reviewed by the appropriate EURO-LABS User selection Panel. Proposals will be evaluated based on scientific merit whilst taking into consideration the [eligibility criteria](https://web.infn.it/EURO-LABS/eligibility-criteria/), availability of the facility and presence of similar facilities in the users’ home country and available funding.

    > **Note**: the USP does not control the allocation of the beam time and the experimental program in the facilities. This is typically done by the CERN scientific committees overseeing each facility. The EURO-LABS transnational access comes as **supplementary** support to the teams for their visits to CERN. </p>
    > The TA applications can arrive anytime.  If the project is already approved and included in the facility schedule, the application will be considered directly. Otherwise, it will be put on hold by the facility coordinator, and activated after the project has been evaluated by the scientific committees and is included in the beamtime schedule of the facility. 

4.	**Outcome of the selection**: The Facility Coordinator will contact you by email regarding the outcome of the selection and will send you back the excel file with the columns "USP approval" filled. 

5.	**Confirmation email**: You as well as all team members listed for TA funding will receive a confirmation e-mail from the secretariat as described below.

6.	**Financial Support**: After your visit at CERN, you should return the excel file to the FC with the columns "Actual" filled with the dates of travel and stay at CERN for each team member. 

7.	**TA report**: When the experiment is completed, you will have to fill a TA report (template [here](expreport.md)) summarizing the objectives and achievements and send it to the facility coordinator within two weeks after completion of the project.

8.	**Publications**: The results will have to be published in a journal with the EURO-LABS acknowledgement. See below:

 ![](../images/EURO-LABS_reftext.jpg)

## Group Members

1. As group member you should provide all required information to the Group Leader to fill the application and user data in the excel file. You should also verify the provided information (training needs, arrival and departure dates) is correct. 

2. **Confirmation e-mail** As soon as the experiment is approved you will receive an email from the secretariat with the following information:

    - An Invitation Letter describing:
        - the conditions of the required access. 
        - the financial support provided
        - requirement to publish results
        - requirement to fill the TA report (only for Group Leaders)
                
        You should sign and return this document prior to your arrival at CERN. Details on the financial support please look [here](travelcosts.md)
    - If requested in the excel file, a form with the training courses to register and attend
    - Information on accommodation
    - Official documents to check, fill and sign by the end of your visit and in order to receive the financial suport:
        - declaration of honour,
        - proof of travel
        - confirmation of beam time (only for Group Leaders) 

> **Note** :
> - most of the documents discussed here are generated automatically from the information provided in the excel file accompanying the application. In case of errors an updated file need to be filled and send to the FC in order to re-generate the documents.

## Access and Training

To gain accesss to some of the CERN facilities, special training needs to be done beforehand. Details in this [page](training.md)
